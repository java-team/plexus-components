#!/bin/sh -e

VERSION=$2
ZIP=../plexus-components-$VERSION.zip
TAR=../plexus-components_$VERSION.orig.tar.gz
DIR=plexus-components-$VERSION

mkdir -p $DIR
# Expand the upstream zip
unzip $ZIP -d $DIR
mv $DIR/sonatype-plexus-components-*/* $DIR
rm -r $DIR/sonatype-plexus-components-*/
# Repack excluding stuff we don't need
# and ensure checksums match when run 
# in different places
# Fix date for file creation - allows consistent
# generating of tarballs from github.com tags.
DATE="Fri Oct 21 12:29:52 BST 2011"
tar -c --exclude '*.jar' --exclude '*.class' \
  --exclude 'plexus-archiver' --exclude 'plexus-cli' --exclude 'plexus-compiler' \
  --exclude 'plexus-interpolation' --exclude 'plexus-io' \
  --exclude 'plexus-i18n' --exclude 'plexus-resources' --exclude 'plexus-velocity' \
  --exclude 'plexus-digest' --exclude 'plexus-interactivity' --exclude 'plexus-swizzle' \
  --exclude 'CVS' --exclude '.svn' --exclude '.git' \
  --mtime="$DATE" --show-stored-names $DIR | \
  gzip -9fn -c - > $TAR

rm -rf $DIR $ZIP

